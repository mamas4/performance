/*global __ENV : true  */
/*
@endpoint: `POST /api/graphql`
@description: Send messages to Duo Chat by calling aiAction GraphQL API
@gitlab_settings: { "instance_level_ai_beta_features_enabled": true }
@gpt_data_version: 1
*/

import http from "k6/http";
import { check, group } from "k6";
import { Rate } from "k6/metrics";

import {
  getRpsThresholds,
  getTtfbThreshold,
  logGraphqlError,
  logError,
} from "../../lib/gpt_k6_modules.js";

export let thresholds = {
  ttfb: { latest: 1000 },
};

export let rpsThresholds = getRpsThresholds(thresholds["rps"]);
export let ttfbThreshold = getTtfbThreshold(thresholds["ttfb"]);
export let successRate = new Rate("successful_requests");

export let options = {
  thresholds: {
    successful_requests: [`rate>${__ENV.SUCCESS_RATE_THRESHOLD}`],
    http_req_waiting: [`p(90)<${ttfbThreshold}`],
    http_reqs: [`count>=${rpsThresholds["count"]}`],
  },
};

// If Service Account PAT is used for GPT, AI tests require real user PAT which can be provided via AI_ACCESS_TOKEN
export const access_token =
  __ENV.AI_ACCESS_TOKEN !== null && __ENV.AI_ACCESS_TOKEN !== undefined
    ? __ENV.AI_ACCESS_TOKEN
    : __ENV.ACCESS_TOKEN;

export function setup() {
  console.log("");
  console.log(
    `RPS Threshold: ${rpsThresholds["mean"]}/s (${rpsThresholds["count"]})`,
  );
  console.log(`TTFB P90 Threshold: ${ttfbThreshold}ms`);
  console.log(
    `Success Rate Threshold: ${parseFloat(__ENV.SUCCESS_RATE_THRESHOLD) * 100}%`,
  );
}

export default function () {
  group("API - Duo Chat aiAction Query", function () {
    let userId = 1;
    let message = "How do I create a template?";

    let params = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };

    let gql_query = `
  mutation chat(
    $agentVersionId: AiAgentVersionID
    $clientSubscriptionId: String
    $currentFileContext: AiCurrentFileInput
  ) {
    aiAction(
      input: {
        chat: {
          resourceId: "gid://gitlab/User/${userId}"
          content: "${message}"
          agentVersionId: $agentVersionId
          currentFile: $currentFileContext
        }
        clientSubscriptionId: $clientSubscriptionId
      }
    ) {
      requestId
      errors
    }
  }`;

    let res = http.post(
      `${__ENV.ENVIRONMENT_URL}/api/graphql`,
      JSON.stringify({ query: gql_query }),
      params,
    );

    // The requestId check is currently failing with "TypeError: Cannot read property 'requestId' of undefined"
    // This is due to Rate Limiting.
    const checkOutput = check(res, {
      "is status 200": (r) => r.status === 200,
      "verify response request-id is not null": (r) =>
        typeof JSON.parse(r.body).data !== "undefined" &&
        typeof JSON.parse(r.body).data.aiAction !== "undefined" &&
        typeof JSON.parse(r.body).data.aiAction.requestId !== "undefined",
    });
    checkOutput
      ? successRate.add(true)
      : (successRate.add(false), logError(res));

    const graphQLErrors = JSON.parse(res.body).errors;
    graphQLErrors
      ? (successRate.add(false), logGraphqlError(graphQLErrors))
      : successRate.add(true);
  });
}
